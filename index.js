//express package was imported as express using require()
const express = require("express");
//invoked express package to create a server/api and saved it in variable which we can refer to later to create routes.
const app = express();
//variable for port assignment
const port = 4000;
//used the listen() method of express to assign a port to our server and send a message

//express.json is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from the request.

//app.use() is a method used to run another function or method for our expressjs api.
//it is used to run middlewares (functions that add features to our application)
app.use(express.json())


// Mock Collection for Courses
let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	},

];

let users = [
	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}

]


//creating a route in Express:
//access express to have access to its route methods.

/*

	app.method('/endpoint',(request,response)=>{
	
		//send() is a method similar to end() that it sends the data/message and ends the response.
		//it also automatically creates and adds the headers.

		response.send

	})

*/


app.get('/',(req,res)=>{

	res.send("Hello from our first ExpressJS route!");

})

app.post('/',(req,res)=>{

	res.send("Hello from our first ExpressJS Post Method Route!")

})

app.put('/',(req,res)=>{

	res.send("Hello from a put method route!")

})

app.delete('/',(req,res)=>{

	res.send("Hello from a delete method route!")

})

app.get('/courses',(req,res)=>{

	res.send(courses);

})

//create a route to be able to add a new course from an input from a request.

app.post('/courses',(req,res)=>{

	//with express.json() the data stream has been captured, the data input has been parsed into a JS object

	//note: Every time you need to access or use the request body, log it in the console first
	// console.log(req.body)//object
	//request/req.body contains the body of the request or the input passed

	//Add the input as req.body into our courses array
	courses.push(req.body);

	// console.log(courses);

	res.send(courses);


})


/*
	ACTIVITY
*/

app.get('/users',(req,res)=>{

	res.send(users);

})

app.post('/users',(req,res)=>{

	users.push(req.body);

	res.send(users);

})

app.delete('/users',(req,res)=>{

	users.pop();

	res.send(users);

})



app.listen(port,() => console.log(`Express API running at port 4000`))